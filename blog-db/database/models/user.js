var mongoose = require('mongoose');
var user = mongoose.Schema({
    firstname:String,
    lastname:String,
    username:String,
    email:String,
    password:String,
    repeatpassword:String,
    phone:Number,
    date:String,
    gender:String
})

module.exports = mongoose.model('User',user); 