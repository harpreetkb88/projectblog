var mongoose = require('mongoose');
var blog = mongoose.Schema({
    userId:String,
    title:String,
    categoryId:String,
    subCategoryId:String,
    blogText:String,
    imageArr:Array,
    date:String,
    thumbImage:String,
   
});

module.exports= mongoose.model('Blog',blog);
