let bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
const nodemailer = require("nodemailer"); 


const encryptPassword = async (hashedPassword) => {

    return new Promise((resolve, reject) => {

        bcrypt.hash(hashedPassword, 10, (err, hash) => {
            if (err) {
                reject(err) 
            }
            else {
                resolve(hash)
            }
        });
    })
}

const comparePassword = async (password, hash) => {
    return new Promise((resolve, reject) => {
        bcrypt.compare(password, hash, (err, res) => {
            if (err) { 
                reject(err)
            }
            else {
                resolve(res)
            }
        })
    })
}

const generateJwt = async (id) => {
    return new Promise((resolve, reject) => {
        let expiry = new Date();
        expiry.setDate(expiry.getDate() + 7);
        let token = jwt.sign({
            _id: id,
            exp: parseInt(expiry.getTime())
        }, "Secret")
        resolve(token)
        reject(err)
    })
}

const verifyJwt = () => {
    try {
        const token = jwt.verify(token, "Secret")
        if (token) {
            return true;
        }
        else
            throw new Error;
    }
    catch{
        return false;
    }
}


const mailFunc= async(email)=>{
    let transporter = nodemailer.createTransport({
        host: "smtp.gmail.com",
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
          user: 'hunnykb1998@gmail.com', // generated ethereal user
          pass: 'hello12345@', // generated ethereal password
        },
      });
    
      // send mail with defined transport object 
      let info = await transporter.sendMail({
        from: "hunnykb1998@gmail.com", // sender address
        to: `${email}`, // list of receivers
        subject: "Hello ✔", // Subject line
        text: "Hello world?", // plain text body
        html: `<b>Hello world?</b>
                       <p>qwertyuiop<p> `, // html body
      });
}



module.exports = {
    encryptPassword, comparePassword, generateJwt, verifyJwt, mailFunc
}