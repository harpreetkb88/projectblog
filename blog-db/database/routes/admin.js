var express = require('express');
var router = express.Router();

var Admin = require('../models/admin');
let { encryptPassword, comparePassword, generateJwt, verifyJwt } = require('../utils/utils');


/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('admin', { title: 'Express' });
}); 

/////////////////////////////////////admin-register////////////////////////////////////

router.post('/admin-register', async (req, res) => {
  try {
    const emailChk = await Admin.findOne({ email: req.body.email }).exec();
    if (emailChk)
      throw new Error('Email is already Registered');

    let hashedPassword = await encryptPassword(req.body.password);
    req.body.password = hashedPassword;

    const newRegister = await new Admin(req.body).save();

    if (newRegister)
      res.json({ message: "Registered Successfully", success: true })
    else
      throw new Error('an err occured during registering')
  }
  catch (err) {
    console.error(err);
    if (err.message)
      res.json({ message: err.message, data: err, success: false });

    else
      res.json({ message: 'error', data: err, success: false })
  }

}) 

router.post('/admin-login', async (req, res) => {
  try {
    const admin = await Admin.findOne({ username: req.body.username }).exec();
    if ( admin ) {
      const passwordChk = await comparePassword(req.body.password, admin.password)
      if (passwordChk) {
        const token = await generateJwt(admin._id);
        res.json({ message: "successfully Logged in", data: token, success: true });
      }
      else {
        throw new Error('Check your credentials');
      }
    }
    else {
      throw new Error('Check your credentials');
    }
  }
  catch (err) {
    console.error(err);
    if (err.message)
      res.json({ message: err.message, data: err, success: false });

    else
      res.json({ message: 'error', data: err, success: false })
  }
})

module.exports = router;
