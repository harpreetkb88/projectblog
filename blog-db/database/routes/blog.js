var express = require('express');
var router = express.Router();

var Blog = require('../models/blog');
let { encryptPassword, comparePassword, generateJwt, verifyJwt, mailFunc } = require('../utils/utils');

router.get('/', function (req, res, next) {
    res.render('blog', { title: 'Express' });
  }); 



/* Add new Blogs */
router.post('/addBlog', async (req, res) => {
    try { 
        // const checkBlog = await Blog.findOne({name:{'$regex': req.body.name,$options:'i'}}).exec()
        // if (checkBlog) {
        //     throw new Error("Blog already exists");
        // }

        const newBlog = await new Blog(req.body).save()
        if (newBlog) {
            res.json({ message: "Blog added Sucessfully", success: true })
        }
        else {
            throw new Error("Blog not added")
        }


    } 
    catch (err) {
        if (err.message)
            res.json({ message: err.message, success: false })
        else
            res.json({ message: 'Error', success: false })
    }
});


//////////////get all Blogs
router.get('/getAllBlogs', async (req, res) => {
    try {
        const allBlogs = await Blog.find().exec();
        res.json({ message: "all Blogs sucess", data: allBlogs, success: true })
    }
    catch (err) {
        if (err.message)
            res.json({ message: err.message, success: false })
        else
            res.json({ message: 'Error', success: false })
    }
});

router.patch('/updateBlog/:id', async (req, res) => {
    try {
      const blog = await Blog.findByIdAndUpdate(req.params.id,req.body).exec()
      if(blog){
        res.json({message:"Blog successfully updated", data:blog, success:true})
      }
      else{
        throw new Error;
      }
    }
    catch (err) {
      console.error(err);
      if (err.message)
        res.json({ message: err.message, data: err, success: false });
  
      else
        res.json({ message: 'error', data: err, success: false })
    }
  })

  router.delete('/deleteBlog/:id', async (req, res) => {
    try {
      const blog = await Blog.findByIdAndDelete(req.params.id).exec()
      if(blog){
        res.json({message:"blog deleted", data:blog, success:true})
      }
      else{
        throw new Error;
      }
    }
    catch (err) {
      console.error(err);
      if (err.message)
        res.json({ message: err.message, data: err, success: false });
  
      else
        res.json({ message: 'error', data: err, success: false })
    }
  })


router.get('getSpecificBlog/:id', async (req,res)=>{
    try {
        const specificBlog = await Blog.findById(req.params.id).exec()
        res.json({ message: "0000", data: specificBlog, success: true })
    }
    catch (err) {
        if (err.message)
            res.json({ message: err.message, success: false })
        else
            res.json({ message: 'Error', success: false })
    }
})


module.exports=router;