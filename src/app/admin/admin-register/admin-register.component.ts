import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { ConfirmedValidator} from '../../confirmed.validator';
import { from } from 'rxjs';
import { AdminRegisterService } from '../../services/admin-register/admin-register.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router'; 

@Component({
  selector: 'app-admin-register',
  templateUrl: './admin-register.component.html',
  styleUrls: ['./admin-register.component.css']
})
export class AdminRegisterComponent implements OnInit {
  registerForm:FormGroup
  constructor(private fb:FormBuilder, private register:AdminRegisterService, private router:Router) { }
 

  ngOnInit(): void { 

    this.registerForm=this.fb.group({
      firstname:[''], 
      lastname:[''],
      username:[''],
      email:[''],
      password:[''],
      repeatpassword:[''],
      phone:[''],
      date:[''],
      gender:[''],
     
    } )

  }
  

  async Register(){
    const res:any = await this.register.Register(this.registerForm.value)
    if(res.success){
      // alert(res.message)
      Swal.fire(
        'Good job!',
        'Registered successfully !',
       
      )
      this.router.navigateByUrl('/admin-login')
    }
    else
    // alert(res.message)
    Swal.fire(
     'username is already Registered !',
    )

  }


}
