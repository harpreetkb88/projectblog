import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms'
import { AdminLoginService } from '../../services/admin-login/admin-login.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {
  loginForm: FormGroup
  constructor(private login:AdminLoginService, private fb:FormBuilder, private router:Router) { }

  ngOnInit(): void {
    this.loginForm=this.fb.group({
      username:[''],
      password:['']
    })
  }
  async Login(){
    const res:any = await this.login.login(this.loginForm.value)
    if(res.success){
              // alert(res.message) 
              Swal.fire(
              'Good job!',
             'Login successfully !',
             
           )
         }
         else
            //  alert(res.message)
          Swal.fire(
           'Check your credentials',
           
          )
        
    await this.login.setJwt(res.data);
    this.router.navigateByUrl('/admin')
  
  
  }
  

}

