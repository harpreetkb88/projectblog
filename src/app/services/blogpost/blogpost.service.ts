import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
 

@Injectable({
  providedIn: 'root'
})
export class BlogpostService {

  url = 'http://localhost:3000';
  constructor(private http: HttpClient) { }

 

  async Blog(formData){
    return this.http.post(`${this.url}/blog/addblog`, formData).toPromise();
  }

  async getUserBlogs() {
    return this.http.get(`${this.url}/blog/getAllBlogs`).toPromise();
  }

  async deleteBlog(id) {
    return this.http.delete(`${this.url}/blog/deleteBlog/${id}`).toPromise();
  }

  async updateBlog(id, formData) {
    return this.http.patch(`${this.url}/blog/updateBlog/${id}`, formData).toPromise();
  }

  async getSpecificUser(id){
    return this.http.get(`${this.url}/blog/getSpecificUser/${id}`).toPromise();
  }


}
