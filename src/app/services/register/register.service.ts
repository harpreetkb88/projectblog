import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root' 
})
export class RegisterService {

  url="http://localhost:3000"

  constructor(private http:HttpClient) { }

  async Register(formData){
    return this.http.post(`${this.url}/register`, formData).toPromise();
  }
}
