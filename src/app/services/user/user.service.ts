import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  url = 'http://localhost:3000';

  // async getId() {
  //   return this.http.get(`${this.url}/getUser`).toPromise();
  // }

  async getUser() {
    return this.http.get(`${this.url}/getUser`).toPromise();
  }

  async deleteUser(id) {
    return this.http.delete(`${this.url}/deleteUser/${id}`).toPromise();
  }

  async updateUser(id, formData) {
    return this.http.patch(`${this.url}/updateUser/${id}`, formData).toPromise();
  }

  async getSpecificUser(id) {
    return this.http.get(`${this.url}/getSpecificUser/${id}`).toPromise();
  }

  getId() {
    let token = localStorage.getItem("userToken");
    if (token) {
      let payload = token.split('.')[1]

      payload = window.atob(payload);
      // console.log(payload[])
      let abc = payload.toString();
      let abcd = abc.slice(8, 32)
      return abcd;
    }
  }
}



