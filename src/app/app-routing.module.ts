import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HeaderComponent } from './index/header/header.component';
import { HomeComponent } from './index/home/home.component';
import { AboutComponent } from './index/about/about.component';
import { ContactComponent } from './index/contact/contact.component';
import { FooterComponent } from './index/footer/footer.component';

import { LoginComponent } from './index/login/login.component';
import { RegisterComponent } from './index/register/register.component';
import { BlogsComponent } from './index/blogs/blogs.component';
import { AdminLoginComponent } from './admin/admin-login/admin-login.component';
import { AdminRegisterComponent } from './admin/admin-register/admin-register.component';
import { AdminComponent } from './admin/admin/admin.component';
import { UserDashboardComponent } from './user/user-dashboard/user-dashboard.component';
import { UserLoginGuard } from './services/login/user-login.guard';
import { AdminLoginGuard } from './services/admin-login/admin-login.guard';
import { AdminAddCategoryComponent } from './admin/admin-add-category/admin-add-category.component';
import { AdminAddSubCategoryComponent } from './admin/admin-add-sub-category/admin-add-sub-category.component';
import { UserProfileComponent } from './user/user-profile/user-profile.component';
import { BlogpostComponent } from './user/blogpost/blogpost.component';



const routes: Routes = [
  {path:'',component:HomeComponent},
  {path:'home',component:HomeComponent},
  {path:'blogs',component:BlogsComponent},
  {path:'about',component:AboutComponent},
  {path:'contact',component:ContactComponent},
  {path:'login',component: LoginComponent},
  {path:'register',component:RegisterComponent},
  // {path:'service',component:ServiceComponent,  canActivate: [UserLoginGuard] },
  {path:'footer',component:FooterComponent},
  {path:'admin-login',component:AdminLoginComponent ,  },
  {path:'admin-register',component:AdminRegisterComponent ,  canActivate: [AdminLoginGuard]},
  {path:'admin',component:AdminComponent, canActivate: [AdminLoginGuard]},
  {path:'user-dashboard',component: UserDashboardComponent,  canActivate: [UserLoginGuard]},
  {path:'admin-add-category',component:AdminAddCategoryComponent},
  {path:'admin-add-sub-category',component: AdminAddSubCategoryComponent},
  {path:'user-profile',component:UserProfileComponent },
  {path:'blogpost',component:BlogpostComponent,  canActivate: [UserLoginGuard] }
  

]; 

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
