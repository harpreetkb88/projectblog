import { Component, OnInit } from '@angular/core';
import { BlogpostService } from '../../services/blogpost/blogpost.service';
import { UserService } from '../../services/user/user.service';

@Component({
  selector: 'app-blogs',
  templateUrl: './blogs.component.html',
  styleUrls: ['./blogs.component.css']
})
export class BlogsComponent implements OnInit {

id;
userBlogs;
modalId;

  constructor( private blog:BlogpostService, private user:UserService) { }

  ngOnInit(): void {

    // this.id =this.user.getUser(); 
    this.getUserBlogs();

  }

  setModalId(id){
    this.modalId=id; 
  }


  async getUserBlogs( ){

    const res: any= await this.blog.getUserBlogs();
    if (res.success){
      this.userBlogs=res.data;
      console.log(res.data);

    }
    else{
      alert(res.message)
    }
  }

}
