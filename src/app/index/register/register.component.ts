import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { ConfirmedValidator} from '../../confirmed.validator';
import { from } from 'rxjs';
import { RegisterService } from '../../services/register/register.service';
import Swal from 'sweetalert2'; 
import { Router } from '@angular/router'; 

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm:FormGroup
  constructor(private fb:FormBuilder, private register:RegisterService, private router:Router) { }
 

  ngOnInit(): void {  

    this.registerForm=this.fb.group({
      firstname:[''], 
      lastname:[''],
      username:[''],
      email:['',[Validators.required, Validators.email]],
      password:['',[Validators.required]],
      repeatpassword:['',[Validators.required]],
      phone:['',[Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
      date:[''],
      gender:[''],
     
    } , { validator:ConfirmedValidator('password','repeatpassword')})

  }
  get f(){
    return this.registerForm.controls;
  }

  async Register(){
    const res:any = await this.register.Register(this.registerForm.value)
    if(res.success){
      // alert(res.message)
      Swal.fire(
        'Good job!',
        'Registered successfully !',
       
      )
      this.router.navigateByUrl('/login')
    }
    else 
    // alert(res.message)
    Swal.fire(
     'Email is already Registered !',
    )
    
  }

}
