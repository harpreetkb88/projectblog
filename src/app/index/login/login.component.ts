
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms'
import { LoginService } from '../../services/login/login.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { UserService } from '../../services/user/user.service';
import { UserLoginGuard } from '../../services/login/user-login.guard';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
   id; 
   emailget;
  loginForm: FormGroup
  constructor(private login:LoginService, private fb:FormBuilder, private router:Router , private user:UserService) { }

  ngOnInit(): void {
    this.loginForm=this.fb.group({
      username:[''],
      password:['']
    })
  }

  async Login(){
   
   


    const res:any = await this.login.login(this.loginForm.value)
    if(res.success){ 
              // alert(res.message)
              Swal.fire(
              'Good job!',
             'Login successfully !',
             
           )
    }
        
         else
            //  alert(res.message)
          Swal.fire(
           'Check your credentials',
           
          )
    
    await this.login.setJwt(res.data);
   
    this.router.navigateByUrl(`/user-dashboard`);
  
    
  }

}
