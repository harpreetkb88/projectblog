import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './index/header/header.component';
import { HomeComponent } from './index/home/home.component';
import { AboutComponent } from './index/about/about.component';
import { FooterComponent } from './index/footer/footer.component';

import { ContactComponent } from './index/contact/contact.component';
import { LoginComponent } from './index/login/login.component';
import { RegisterComponent } from './index/register/register.component';
import { BlogsComponent } from './index/blogs/blogs.component';
import { AdminComponent } from './admin/admin/admin.component';
import { AdminLoginComponent } from './admin/admin-login/admin-login.component';
import { AdminRegisterComponent } from './admin/admin-register/admin-register.component';
import { UserDashboardComponent } from './user/user-dashboard/user-dashboard.component';

import { AngularEditorModule } from '@kolkov/angular-editor';
import { AdminAddCategoryComponent } from './admin/admin-add-category/admin-add-category.component';
import { AdminAddSubCategoryComponent } from './admin/admin-add-sub-category/admin-add-sub-category.component';
import { UserProfileComponent } from './user/user-profile/user-profile.component';
import { BlogpostComponent } from './user/blogpost/blogpost.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    AboutComponent, 
    FooterComponent,
    
    ContactComponent,
    LoginComponent,
    RegisterComponent,
    BlogsComponent,
    AdminComponent,
    AdminLoginComponent,
    AdminRegisterComponent,
    UserDashboardComponent,
    AdminAddCategoryComponent,
    AdminAddSubCategoryComponent,
    UserProfileComponent,
    BlogpostComponent,
   
  ], 
  imports: [ 
    BrowserModule,
    AppRoutingModule,
    FormsModule, ReactiveFormsModule, 
    AngularEditorModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
