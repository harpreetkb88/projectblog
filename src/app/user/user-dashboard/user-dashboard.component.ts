import { Component, OnInit } from '@angular/core';
import { BlogpostService } from '../../services/blogpost/blogpost.service';
import { UserService } from '../../services/user/user.service';
import * as $ from 'jquery';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { LoginService } from 'src/app/services/login/login.service';

@Component({
  selector: 'app-user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: ['./user-dashboard.component.css']
})
export class UserDashboardComponent implements OnInit {

blogForm: FormGroup;
id;
userBlogs;
modalId;
specificUser;
blogs;

  constructor( private fb: FormBuilder, private blog:BlogpostService, private user:UserService ,private router: Router, private login:LoginService) { }

  ngOnInit(): void {
  
    this.id=this.user.getId();
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
  });
  
  this.blogForm = this.fb.group({
    title: [''],
    categoryId: [''],
    subCategoryId: [''],
    userId: [this.id],
    blogText: [''],
    imageArr: []

  });


  this.getSpecificUser(this.id);
  this.getUserBlogs();
  }

  setModalId(id){
    this.modalId=id; 
  }

//   async deleteUser(id){
//     const res:any = await this.user.deleteUser(id)
//     if(res.success) {
//       alert(res.message)
//     }
//     else{
//       alert(res.message)
//     }
//     this.ngOnInit();
//   }

// }


  async updateBlog(){
    const res:any= await this.blog.updateBlog(this.modalId,this.blogForm.value)
    if(res.success) {
      alert(res.message)
    }
    else{
      alert(res.message)
    }
    window.location.reload();
  }

  
  async logout() {
    this.login.removeJwt();
    this.router.navigateByUrl('/login');
  }

  async getUserBlogs( ){

    const res: any= await this.blog.getUserBlogs();
    if (res.success){
      this.blogs=res.data;
      this.userBlogs = this.blogs.filter(el => el.userId == this.id)
    }
    else{
      alert(res.message)
    }
  }

  async getSpecificUser(id) {
    const res: any = await this.user.getSpecificUser(id);
    if (res.success){
      this.specificUser = res.data;
      console.log(id);
    }
    else{
      alert(res.message);
    }

  }


  async deleteUser(id){
    const res:any = await this.blog.deleteBlog(id)
    if(res.success) {
      alert(res.message)
    }
    else{
      alert(res.message)
    }
    this.ngOnInit();
  }



}
