import { Component, OnInit } from '@angular/core';
import { BlogpostService } from '../../services/blogpost/blogpost.service';
import { LoginService } from '../../services/login/login.service';
import { UserService } from '../../services/user/user.service';
import { Router } from '@angular/router';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { FormBuilder, FormGroup } from '@angular/forms';
import Swal from 'sweetalert2';
import { CategoryService } from '../../services/category/category.service';
import { SubSubCategoryService } from '../../services/subCategory/sub-category.service';


@Component({
  selector: 'app-blogpost',
  templateUrl: './blogpost.component.html',
  styleUrls: ['./blogpost.component.css']
})
export class BlogpostComponent implements OnInit {

  constructor(private fb: FormBuilder, private router: Router,
    private category: CategoryService, private subCategory: SubSubCategoryService,
    private blog: BlogpostService, private user: UserService, private login: LoginService) { }

  allCategoryObj;
  allUserObj;
  allSubCategoryObj;
  tempAllSubCategoryObj;
  tempImageArr: any = [];
  displayArr: any = [];
  blogForm: FormGroup;
  id;

  ngOnInit(): void {

    this.id = this.user.getId()

    console.log(this.id)
    this.getCategorys();
    this.getSubCategorys();

    this.blogForm = this.fb.group({
      title: [''],
      categoryId: [''],
      subCategoryId: [''],
      userId: [this.id],
      blogText: [''],
      imageArr: []

    });


  }


  async logout() {
    this.login.removeJwt();
    this.router.navigateByUrl('/login');
  }

  async getCategorys() {
    const res: any = await this.category.getCategorys();
    this.allCategoryObj = res.data;
  }

  async getSubCategorys() {
    const res: any = await this.subCategory.getSubCategorys();
    this.allSubCategoryObj = res.data;
    this.tempAllSubCategoryObj = res.data;

  }



  uploadImage(event) {

    let reader = new FileReader();

    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);

      reader.onload = () => {
        this.tempImageArr.push(reader.result);
        this.displayArr.push(event.target.value.split(/(\\|\/)/g).pop());
        this.blogForm.patchValue({
          imageArr: this.tempImageArr
        })
        // console.log(this.specificationForm.value);
      }
    }

  }
  //////////////////////////////deleting  image from array
  deleteImage(i) {
    this.tempImageArr.splice(i, 1)
    this.displayArr.splice(i, 1)
    this.blogForm.patchValue({
      imageArr: this.tempImageArr
    })
  }



  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: 'auto',
    minHeight: '200px',
    maxHeight: 'auto',
    width: 'auto',
    minWidth: '0',
    translate: 'yes',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Enter text here...',
    defaultParagraphSeparator: '',
    defaultFontName: '',
    defaultFontSize: '',
    fonts: [
      { class: 'arial', name: 'Arial' },
      { class: 'times-new-roman', name: 'Times New Roman' },
      { class: 'calibri', name: 'Calibri' },
      { class: 'comic-sans-ms', name: 'Comic Sans MS' }
    ],
    customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    uploadUrl: 'v1/image',
    uploadWithCredentials: false,
    sanitize: true,
    toolbarPosition: 'top',
    toolbarHiddenButtons: [
      ['bold', 'italic'],
      ['fontSize']
    ]
  };

  async Blog() {
    const res: any = await this.blog.Blog(this.blogForm.value)
    if (res.success) {
      // alert(res.message)
      Swal.fire(
        'Good job!',
        'Blog successfully posted!',

      )

    }
    else {
      alert(res.message)
    }
    window.location.reload();

  }


}


