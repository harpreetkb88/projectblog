import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user/user.service';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit { 

  constructor(private user: UserService,private fb:FormBuilder) { }

  currentUser;
  id;
  userArr;
  modalId;
  
  registerForm:FormGroup;

  
  setModalId(id){
    this.modalId=id; 
  }


  ngOnInit(): void {
    this.id = this.user.getId();
    this.getSpecificUser();
    this.registerForm=this.fb.group({
      firstname:[''],
      lastname:[''],
      username:[''],
      email:[''],
      password:['']
    })

  }


  
  async updateUser(){
    const res:any= await this.user.updateUser(this.modalId,this.registerForm.value)
    if(res.success) {
      alert(res.message)
    }
    else{
      alert(res.message)
    }
    window.location.reload();
  }


  async deleteUser(id){
    const res:any = await this.user.deleteUser(id)
    if(res.success) {
      alert(res.message)
    }
    else{
      alert(res.message)
    }
    this.ngOnInit();
  }


  async getSpecificUser() {
    const res: any = await this.user.getSpecificUser(this.id);
    if (res.success) {

      this.currentUser = res.data;

    }
    else {
      alert(res.message)
    }
  }


}
